module gitlab.com/streetwear/data-center

go 1.16

//replace gitlab.com/streetwear/api => ../api
//replace gitlab.com/streetwear/basic => ../basic

require (
	github.com/go-kratos/kratos/contrib/config/nacos/v2 v2.0.0-20220711115126-1451b9e0c0e3
	github.com/go-kratos/kratos/contrib/registry/zookeeper/v2 v2.0.0-20220711115126-1451b9e0c0e3
	github.com/go-kratos/kratos/v2 v2.4.0
	github.com/go-kratos/swagger-api v1.0.1
	github.com/go-zookeeper/zk v1.0.2
	github.com/golang/protobuf v1.5.2
	github.com/jinzhu/copier v0.3.5
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/nacos-group/nacos-sdk-go v1.1.0
	github.com/spf13/cobra v1.5.0
	gitlab.com/streetwear/basic v1.2.6
	golang.org/x/oauth2 v0.0.0-20220223155221-ee480838109b // indirect
	google.golang.org/api v0.30.0
	google.golang.org/genproto v0.0.0-20220519153652-3a47de7e79bd
	google.golang.org/grpc v1.46.2
	google.golang.org/protobuf v1.28.0
	gorm.io/datatypes v1.0.7
	gorm.io/gorm v1.23.6
	gorm.io/plugin/dbresolver v1.2.1
)
