package main

import (
	"github.com/go-kratos/kratos/contrib/registry/zookeeper/v2"
	"github.com/go-kratos/kratos/v2"
	"github.com/go-zookeeper/zk"
	"gitlab.com/streetwear/basic/logx"
	"gitlab.com/streetwear/data-center/internal/conf"
	"gitlab.com/streetwear/data-center/internal/server"
	"runtime"
	"time"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	// 初始化必须的服务
	InitServer()

	// 启动服务
	StartServer()
}

func InitServer() {
	conf.InitLog()
	conf.InitConf()
	server.MysqlRegister()
}

func StartServer() {
	// zookeeper
	zkConnection, _, err := zk.Connect([]string{"114.132.230.6:2181"}, time.Second*10)
	if err != nil {
		logx.Error(err)
	}
	r := zookeeper.New(zkConnection)
	//logger := log.DefaultLogger

	httpSrv := server.NewHTTPServer()
	grpcSrv := server.NewGRPCServer()
	app := kratos.New(
		kratos.Name(conf.GetConfig().ServerName.DataCenter),
		kratos.Logger(logx.GetLogger()),
		kratos.Server(
			grpcSrv,
			httpSrv,
		),
		kratos.Registrar(r),
	)
	if err := app.Run(); err != nil {
		logx.Info(err.Error())
	}
}
