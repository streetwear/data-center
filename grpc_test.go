package main

import (
	"context"
	"encoding/json"
	"fmt"
	nacos "github.com/go-kratos/kratos/contrib/config/nacos/v2"
	"github.com/go-kratos/kratos/contrib/registry/zookeeper/v2"
	kconfig "github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	transgrpc "github.com/go-kratos/kratos/v2/transport/grpc"
	"github.com/go-zookeeper/zk"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"gitlab.com/streetwear/basic/logx"
	pd "gitlab.com/streetwear/data-center/api/account"
	config "gitlab.com/streetwear/data-center/internal/conf"
	"gitlab.com/streetwear/data-center/internal/data"
	"testing"
	"time"
)

var zkConn *zookeeper.Registry
var conf *config.Config

func InitConf() {
	conf = &config.Config{
		NacosGroup:  "data-center",
		NacosDataID: "data-center",
		Mode:        "local",
	}

	// 从nacos获取配置，解析到config
	sc := []constant.ServerConfig{
		*constant.NewServerConfig("114.132.230.6", 8848),
	}
	cc := constant.ClientConfig{
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./log",
		CacheDir:            "./cache",
		LogLevel:            "debug",
	}
	client, err := clients.NewConfigClient(
		vo.NacosClientParam{
			ClientConfig:  &cc,
			ServerConfigs: sc,
		},
	)
	c := kconfig.New(
		kconfig.WithSource(
			nacos.NewConfigSource(client, nacos.WithGroup(conf.NacosDataID), nacos.WithDataID(conf.NacosDataID)),
		),
		kconfig.WithDecoder(func(kv *kconfig.KeyValue, v map[string]interface{}) error {
			return json.Unmarshal(kv.Value, &v)
		}),
	)
	if err := c.Load(); err != nil {
		return
	}
	name, err := c.Value("dbServer").String()
	if err != nil {
		log.Errorf(err.Error())
	}
	fmt.Println("get value", name)

	if err := c.Scan(&conf); err != nil {
		return
	}
	log.Info(conf)
}

func init() {
	InitConf()
	config.SetConfig(conf)
	config.InitLog()
	logx.Info("start")
	data.InitDB()
	//go StartServer()

	time.Sleep(time.Second * 5)
	zkConnection, _, err := zk.Connect([]string{"114.132.230.6:2181"}, time.Second*10)
	if err != nil {
		logx.Error(err)
	}
	zkConn = zookeeper.New(zkConnection)
}
func Benchmark_GrpcFunction(b *testing.B) {
	b.StopTimer()
	//调用该函数停止压力测试的时间计数
	//做一些初始化的工作,例如读取文件数据,数据库连接之类的,
	//这样这些时间不影响我们测试函数本身的性能

	b.StartTimer()           //重新开始时间
	for i := 0; i < 1; i++ { //use b.N for looping
		callGrpc()
	}
}

//func TestGrpc(t *testing.T) {

func callGrpc() {
	conn, err := transgrpc.DialInsecure(
		context.Background(),
		transgrpc.WithEndpoint(fmt.Sprintf("discovery:///%s", conf.ServerName.DataCenter)),
		transgrpc.WithMiddleware(
			recovery.Recovery(),
		),
		transgrpc.WithDiscovery(zkConn),
	)

	if err != nil {
		panic(err)
	}
	defer conn.Close()
	client := pd.NewAccountClient(conn)
	reply, err := client.Get(context.Background(), &pd.GetRequest{Userid: 0})
	if err != nil {
		logx.Error(err)
		logx.Error(err.Error())
	}
	logx.Info(reply)
}

func TestPath(t *testing.T) {
}
