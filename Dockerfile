FROM docker.io/alpine
ADD ./cmd /go/main/
EXPOSE 8081:8081
ENTRYPOINT ["/go/main/data-center"]