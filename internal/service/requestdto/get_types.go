package requestdto

type GetUserReq struct {
	UserID int `form:"userid"`
}

type PageQuery struct {
	Page int `form:"page" binding:"required,gte=1"`
	Row  int `form:"row" binding:"required,gte=1"`
}

type PageUserReq struct {
	PageQuery
	Userid       int    `form:"userID"`
	Account      string `form:"account,optional"`
	Username     string `form:"username,optional"`
	Phone        string `form:"phone,optional"`
	Email        string `form:"email,optional"`
	Position     string `form:"position,optional"`
	RoleId       int    `form:"roleID,optional"`
	Sex          int    `form:"sex,optional"`
	DepartmentId int    `form:"departmentId,optional"`
	Status       *int   `form:"status,optional"`
}

type PageRoleReq struct {
	PageQuery
	RoleId   int    `form:"roleID,optional"`
	RoleName string `form:"roleName,optional"`
	Remark   string `form:"remark,optional"`
}

type GetRoleReq struct {
	RoleId int `form:"roleID"`
}

type GetDepartReq struct {
	// DepartReq
}

type DepartmentListHandlerReq struct {
	Pid  *int64 `form:"pid"`
	Name string `form:"name"`
}

type PageDepartReq struct {
	PageQuery
	DepID  int    `form:"depID,optional"`
	Name   string `form:"name,optional"`
	Remark string `form:"remark,optional"`
	Pid    int    `form:"pid,optional"`
}

type UsedAreaSelectListReq struct {
	Pid  *int   `form:"pid,omitempty"`
	Name string `form:"name"`
	Code *int   `form:"code,omitempty"`
}
