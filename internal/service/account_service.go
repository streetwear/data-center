package service

import (
	"context"
	"github.com/jinzhu/copier"
	"gitlab.com/streetwear/basic/logx"
	pb "gitlab.com/streetwear/data-center/api/account"
	"gitlab.com/streetwear/data-center/internal/data/account"
)

type AccountService struct {
	pb.UnimplementedAccountServer
}

func (s *AccountService) Get(ctx context.Context, req *pb.GetRequest) (rep *pb.GetReply, err error) {
	accountInfo, err := account.Account{}.GetAccountByUserid(req.Userid)
	if err != nil {
		logx.Error(err.Error())
		return nil, err
	}
	if err = copier.Copy(&accountInfo, &req); err != nil {
		logx.Error(err.Error())
	}
	logx.Info(accountInfo)

	rep = &pb.GetReply{
		AccountInfo: &pb.AccountInfo{},
	}
	if err = copier.Copy(&rep.AccountInfo, &accountInfo); err != nil {
		logx.Error(err.Error())
	}
	return
}

func (s *AccountService) Add(ctx context.Context, req *pb.AddRequest) (rep *pb.AddReply, err error) {
	accountInfo := account.Account{}

	if err = copier.Copy(&accountInfo, &req); err != nil {
		logx.Error(err.Error())
	}

	if err = copier.Copy(&rep, &accountInfo); err != nil {
		logx.Error(err.Error())
	}
	return rep, nil
}
