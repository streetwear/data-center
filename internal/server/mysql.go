package server

import (
	"gitlab.com/streetwear/basic/logx"
	"gitlab.com/streetwear/data-center/internal/data"
	"gitlab.com/streetwear/data-center/internal/data/account"
	"gorm.io/plugin/dbresolver"
)

func MysqlRegister() {
	data.InitDB()
	db := data.GetDBConnection()
	if err := db.Use(dbresolver.Register(dbresolver.Config{}, &account.Account{}, &account.AccountCoupon{},
		&account.AccountVip{}, &account.AccountVipLog{}, &account.Friend{})); err != nil {
		logx.Panic("数据库注册失败")
		return
	}
}
