package server

import (
	"github.com/go-kratos/kratos/v2/middleware/logging"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/transport/grpc"
	"gitlab.com/streetwear/basic/logx"
	"gitlab.com/streetwear/data-center/api/account"
	"gitlab.com/streetwear/data-center/internal/service"
)

func NewGRPCServer() *grpc.Server {
	grpcSrv := grpc.NewServer(
		grpc.Address(":31000"),
		grpc.Middleware(
			logging.Server(logx.GetLogger()),
			recovery.Recovery(),
		),
	)

	accountService := &service.AccountService{}
	account.RegisterAccountServer(grpcSrv, accountService)

	//orderService := &service.OrderService{}
	//order.RegisterOrderServer(grpcSrv, orderService)

	return grpcSrv
}
