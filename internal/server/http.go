package server

import (
	"github.com/go-kratos/kratos/v2/middleware/logging"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/go-kratos/swagger-api/openapiv2"
	"gitlab.com/streetwear/basic/logx"
	"gitlab.com/streetwear/data-center/api/account"
	"gitlab.com/streetwear/data-center/internal/service"
)

func NewHTTPServer() *http.Server {
	httpSrv := http.NewServer(
		http.Address(":31100"),
		http.Middleware(
			logging.Server(logx.GetLogger()),
			recovery.Recovery(),
		),
	)

	openAPIServer := openapiv2.NewHandler()
	httpSrv.HandlePrefix("/q/", openAPIServer)

	accountService := &service.AccountService{}
	account.RegisterAccountHTTPServer(httpSrv, accountService)

	//orderService := &service.OrderService{}
	//order.RegisterOrderHTTPServer(httpSrv, orderService)

	return httpSrv
}
