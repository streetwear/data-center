package account

import (
	"gorm.io/datatypes"
)

// AccountCoupon 优惠券
type AccountCoupon struct {
	CouponID       int            `gorm:"column:coupon_id" json:"couponId"`
	Type           int            `gorm:"column:type" json:"type"`                      // 优惠券类型
	GetTime        datatypes.Date `gorm:"column:get_time" json:"getTime"`               // 获得时间
	ExpirationTime datatypes.Date `gorm:"column:expiration_time" json:"expirationTime"` // 过期时间
}

// TableName get sql table name.获取数据库表名
func (m *AccountCoupon) TableName() string {
	return "account_coupon"
}

// AccountCouponColumns get sql column name.获取数据库列名
var AccountCouponColumns = struct {
	CouponID       string
	Type           string
	GetTime        string
	ExpirationTime string
}{
	CouponID:       "coupon_id",
	Type:           "type",
	GetTime:        "get_time",
	ExpirationTime: "expiration_time",
}
