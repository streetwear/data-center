package account

// Friend [...]
type Friend struct {
	AccountID       int `gorm:"primaryKey;column:account_id" json:"accountId"`
	FriendAccountID int `gorm:"column:friend_account_id" json:"friendAccountId"` // 好友用户ID
	FriendComments  int `gorm:"column:friend_comments" json:"friendComments"`    // 好友备注
}

// TableName get sql table name.获取数据库表名
func (m *Friend) TableName() string {
	return "friend"
}

// FriendColumns get sql column name.获取数据库列名
var FriendColumns = struct {
	AccountID       string
	FriendAccountID string
	FriendComments  string
}{
	AccountID:       "account_id",
	FriendAccountID: "friend_account_id",
	FriendComments:  "friend_comments",
}
