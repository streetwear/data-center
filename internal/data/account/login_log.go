package account

// LoginLog [...]
type LoginLog struct {
	AccountID    int    `gorm:"primaryKey;column:account_id" json:"accountId"`
	LoginIP      string `gorm:"column:login_ip" json:"loginIp"`
	LoginMode    int    `gorm:"column:login_mode" json:"loginMode"`
	LoginAddress string `gorm:"column:login_address" json:"loginAddress"`
}

// TableName get sql table name.获取数据库表名
func (m *LoginLog) TableName() string {
	return "login_log"
}

// LoginLogColumns get sql column name.获取数据库列名
var LoginLogColumns = struct {
	AccountID    string
	LoginIP      string
	LoginMode    string
	LoginAddress string
}{
	AccountID:    "account_id",
	LoginIP:      "login_ip",
	LoginMode:    "login_mode",
	LoginAddress: "login_address",
}
