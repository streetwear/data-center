package account

// AccountVip [...]
type AccountVip struct {
	AccountID int `gorm:"primaryKey;column:account_id" json:"accountId"`
	Integral  int `gorm:"column:integral" json:"integral"`
	Level     int `gorm:"column:level" json:"level"`
}

// TableName get sql table name.获取数据库表名
func (m *AccountVip) TableName() string {
	return "account_vip"
}

// AccountVipColumns get sql column name.获取数据库列名
var AccountVipColumns = struct {
	AccountID string
	Integral  string
	Level     string
}{
	AccountID: "account_id",
	Integral:  "integral",
	Level:     "level",
}
