package account

import (
	"time"
)

// AccountVipLog 积分获取记录
type AccountVipLog struct {
	AccountID     int       `gorm:"primaryKey;column:account_id" json:"accountId"`
	IntegralValue int       `gorm:"column:Integral_value" json:"integralValue"`
	Type          int       `gorm:"column:type" json:"type"`        // 积分类型 1、赠送；2、消费
	GetTime       time.Time `gorm:"column:get_time" json:"getTime"` // 积分获取时间
}

// TableName get sql table name.获取数据库表名
func (m *AccountVipLog) TableName() string {
	return "account_vip_log"
}

// AccountVipLogColumns get sql column name.获取数据库列名
var AccountVipLogColumns = struct {
	AccountID     string
	IntegralValue string
	Type          string
	GetTime       string
}{
	AccountID:     "account_id",
	IntegralValue: "Integral_value",
	Type:          "type",
	GetTime:       "get_time",
}
