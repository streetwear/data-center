package account

import (
	"fmt"
	"gitlab.com/streetwear/data-center/internal/data"
	"time"
)

// Account 账号信息
type Account struct {
	AccountID     int       `gorm:"primaryKey;column:account_id" json:"accountId"` // 用户ID
	AccountNumber string    `gorm:"column:account_number" json:"accountNumber"`    // 登陆账户
	Password      string    `gorm:"column:password" json:"password"`               // 密码
	AccountName   string    `gorm:"column:account_name" json:"accountName"`        // 用户名
	Phone         string    `gorm:"column:phone" json:"phone"`                     // 手机号
	Status        int       `gorm:"column:status" json:"status"`                   // 状态 1:正常 2:删除
	Email         string    `gorm:"column:email" json:"email"`                     // 邮箱
	Sex           int       `gorm:"column:sex" json:"sex"`                         // 性别1女2男0未知
	CreateTime    time.Time `gorm:"column:create_time" json:"createTime"`          // 创建时间
	Image         string    `gorm:"column:image" json:"image"`                     // 用户头像
	Remark        string    `gorm:"column:remark" json:"remark"`
}

// TableName get sql table name.获取数据库表名
func (m *Account) TableName() string {
	return "account"
}

// AccountColumns get sql column name.获取数据库列名
var AccountColumns = struct {
	AccountID     string
	AccountNumber string
	Password      string
	AccountName   string
	Phone         string
	Status        string
	Email         string
	Sex           string
	CreateTime    string
	Image         string
	Remark        string
}{
	AccountID:     "account_id",
	AccountNumber: "account_number",
	Password:      "password",
	AccountName:   "account_name",
	Phone:         "phone",
	Status:        "status",
	Email:         "email",
	Sex:           "sex",
	CreateTime:    "create_time",
	Image:         "image",
	Remark:        "remark",
}

func (m Account) GetAccountByUserid(userid int32) (*Account, error) {
	tx := data.GetDBConnection()
	var resp Account
	where := fmt.Sprintf("%s = ?", AccountColumns.AccountID)
	if err := tx.Where(where, userid).First(&resp).Error; err != nil {
		return nil, err
	}
	return &resp, nil
}

func (m *Account) Create() error {
	tx := data.GetDBConnection()
	return tx.Create(m).Error
}
