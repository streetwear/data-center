package data

import (
	"fmt"
	"gitlab.com/streetwear/basic/ormx"
	"gitlab.com/streetwear/data-center/internal/conf"
	"gorm.io/gorm"
	"sync"
)

var (
	db         *gorm.DB
	mysqlCreat sync.Once
)

// InitDB
// 获取MySQL连接使用本方法，而非从上下文中获取，上下文中的MySqlClient即将删除
func InitDB() {
	if db == nil {
		mysqlCreat.Do(func() {
			config := conf.GetConfig()
			dsn := fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
				config.DBUser,
				config.DBPassword,
				config.DBAddr,
				config.DBAccountName)
			db = ormx.MysqlConnect(dsn, ormx.Info)
		})
	}

}

func GetDBConnection() *gorm.DB {
	return db
}
