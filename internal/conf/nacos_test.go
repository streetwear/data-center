package conf

import (
	"encoding/json"
	"fmt"
	nacos "github.com/go-kratos/kratos/contrib/config/nacos/v2"
	kconfig "github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"testing"
)

func TestNacos(t *testing.T) {

	sc := []constant.ServerConfig{
		*constant.NewServerConfig("114.132.230.6", 8848),
	}
	cc := constant.ClientConfig{
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./log",
		CacheDir:            "./cache",
		LogLevel:            "debug",
	}
	// a more graceful way to create naming client
	client, err := clients.NewConfigClient(
		vo.NacosClientParam{
			ClientConfig:  &cc,
			ServerConfigs: sc,
		},
	)

	getConfig, err := client.GetConfig(vo.ConfigParam{DataId: "base", Group: "local"})
	if err != nil {
		return
	}
	fmt.Println(getConfig)

	c := kconfig.New(
		kconfig.WithSource(
			nacos.NewConfigSource(client, nacos.WithGroup("local"), nacos.WithDataID("base")),
		),
		kconfig.WithDecoder(func(kv *kconfig.KeyValue, v map[string]interface{}) error {
			return json.Unmarshal(kv.Value, &v)
		}),
	)

	if err := c.Load(); err != nil {
		return
	}
	name, err := c.Value("dbServer").String()
	if err != nil {
		log.Errorf(err.Error())
	}
	fmt.Println("get value", name)

	var v map[string]interface{}
	if err := c.Scan(&v); err != nil {
		return
	}
	log.Info(v)
}
