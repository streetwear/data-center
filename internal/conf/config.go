package conf

import (
	"encoding/json"
	"flag"
	nacos "github.com/go-kratos/kratos/contrib/config/nacos/v2"
	kconfig "github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"gitlab.com/streetwear/basic/logx"
)

var config *Config

type (
	Config struct {
		NacosDataID   string `json:"nacosDataID"`
		NacosGroup    string `json:"nacosGroup"`
		Mode          string `json:"mode"`
		Host          string `json:"host"` // 0.0.0.0
		DBAddr        string `json:"dbAddr"`
		DBAccountName string `json:"dbAccountName"`
		DBUser        string `json:"dbUser"`
		DBPassword    string `json:"dbPassword"`
		ZKServer      string `json:"zkServer"`
		ServerName    struct {
			DataCenter string `json:"data-center"`
		} `json:"serverName"`
	}
)

func GetConfig() *Config {
	return config
}

func SetConfig(conf *Config) {
	config = conf
}

func InitConf() {
	config = &Config{
		NacosDataID: "data-center",
		NacosGroup:  "data-center",
	}
	getFlag()
	log.Info(config)

	// 从nacos获取配置，解析到config
	sc := []constant.ServerConfig{
		*constant.NewServerConfig("114.132.230.6", 8848),
	}
	cc := constant.ClientConfig{
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./log",
		CacheDir:            "./cache",
		LogLevel:            "debug",
		NamespaceId:         config.Mode,
	}
	client, err := clients.NewConfigClient(
		vo.NacosClientParam{
			ClientConfig:  &cc,
			ServerConfigs: sc,
		},
	)
	if err != nil {
		panic(err.Error())
		return
	}
	c := kconfig.New(
		kconfig.WithSource(
			nacos.NewConfigSource(client, nacos.WithGroup(config.NacosDataID), nacos.WithDataID(config.NacosGroup)),
		),
		kconfig.WithDecoder(func(kv *kconfig.KeyValue, v map[string]interface{}) error {
			return json.Unmarshal(kv.Value, &v)
		}),
	)
	if err := c.Load(); err != nil {
		panic(err.Error())
		return
	}

	if err := c.Scan(&config); err != nil {
		panic(err.Error())
		return
	}
	log.Info(config)
}

func getFlag() {
	var m string
	flag.StringVar(&m, "m", "local", "Running in which mode , base、dev、release")
	flag.Parse()
	config.Mode = m
}

func InitLog() {
	logx.InitLogger(logx.DebugLevel)
}
